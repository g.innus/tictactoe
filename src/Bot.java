import java.util.Random;

public class Bot {
    private String boardStr = "zzzzzzzzz";

    public void update(Board board){
        int k = 0;
        char[] boardChArr;
        for (int i=0; i<board.getBoard().length; i++) {
            for (int j=0; j<board.getBoard()[0].length; j++) {

                if(board.getBoard()[i][j].getValue()=='0'){
                    boardChArr = boardStr.toCharArray();
                    boardChArr[k]='0';
                    boardStr = String.valueOf(boardChArr);
                }

                if(board.getBoard()[i][j].getValue()=='X'){
                    boardChArr = boardStr.toCharArray();
                    boardChArr[k]='x';
                    boardStr = String.valueOf(boardChArr);
                }
                k++;
            }
        }

    }
    private Cell[] getRow(int i){
        Cell[] row = new Cell[3];
        for (int j=0;j<3;j++){
            row[j]=new Cell();
            row[j].setValue(boardStr.charAt(i*3+j));
            row[j].setI(i);
            row[j].setJ(j);
        }
        return row;
    }
    private Cell[] getCol(int i){
        Cell[] col = new Cell[3];
        for (int j=0;j<3;j++){
            col[j]=new Cell();
            col[j].setValue(boardStr.charAt(j*3+i));
            col[j].setI(j);
            col[j].setJ(i);
        }
        return col;
    }
    private Cell[] getBoard(){
        Cell[] board = new Cell[9];
        for (int j=0;j<9;j++){
            board[j]=new Cell();
            board[j].setValue(boardStr.charAt(j));
            board[j].setI(j/3);
            board[j].setJ(j%3);
        }
        return board;
    }
    public Cell[] getDiag(int i){
        Cell[] diag = new Cell[3];

        if(i==0){
            int strIndex = 0;
            for (int j=0;j<3;j++){
                diag[j]=new Cell();
                diag[j].setValue(boardStr.charAt(strIndex));
                diag[j].setI(j);
                diag[j].setJ(j);
                strIndex = strIndex+4;
            }
        }else {
            int strIndex = 2;
            int k=2;
            for (int j = 0; j < 3; j++) {
                diag[j] = new Cell();
                diag[j].setValue(boardStr.charAt(strIndex));
                diag[j].setI(j);
                diag[j].setJ(k);
                strIndex = strIndex + 2;
                k--;
            }

        }
        return diag;
    }
    private Cell[] getCorners(){
        Cell[] corners = new Cell[4];
        corners[0]=new Cell();
        corners[0].setValue(boardStr.charAt(0));
        corners[0].setJ(0);
        corners[0].setI(0);
        corners[1]=new Cell();
        corners[1].setValue(boardStr.charAt(2));
        corners[1].setJ(2);
        corners[1].setI(0);
        corners[2]=new Cell();
        corners[2].setValue(boardStr.charAt(6));
        corners[2].setJ(0);
        corners[2].setI(2);
        corners[3]=new Cell();
        corners[3].setValue(boardStr.charAt(8));
        corners[3].setJ(2);
        corners[3].setI(2);
        return corners;
    }


    private int getRandEmptyCornerPos(){
        Random r = new Random();
        int pos = -1;
        int count = 0;
        int[] corners = {0, 2, 6, 8};
        count = count(getCorners(), 'z');
        if(count>0){
            int[] emptyCorners = new int[count];
            int i = 0;
            for (int index : corners){
                if(boardStr.charAt(index)=='z'){
                    emptyCorners[i]=index;
                    i++;
                }
            }
            pos = emptyCorners[r.nextInt(count)];
        }
        return pos;
    }

    public int getNextMove(int difficulty){
        int move;

        Random r = new Random();
        if (count(getCorners(), 'z')==4&&count(getBoard(),'x')==0&&difficulty==1) {
            System.out.println("first");
            int[] corners = {0, 2, 6, 8};
            move = corners[r.nextInt(4)];
        }else if(count(getBoard(), 'x')==1&&count(getBoard(),'0')==0&&boardStr.charAt(4)=='z'&&difficulty==1) {
            move = 4;
        }else if(getOnlyMove('0')>-1&&difficulty==1){
            System.out.println("second");
            move = getOnlyMove('0');
        }else if(getOnlyMove('x')>-1&&difficulty==1){
            System.out.println("third");
            move = getOnlyMove('x');
        }else if(count(getCorners(),'x')==2&&boardStr.charAt(4)=='0'&&count(getBoard(),'x')==2&&count(getBoard(),'0')==1&&difficulty==1){
            move = getPosNextToCorner();

        }else if(getRandEmptyCornerPos()>-1&&difficulty==1){
            move = getRandEmptyCornerPos();
        }
        else{
            System.out.println("rand");
            move = r.nextInt(9);
            while (!(boardStr.charAt(move)=='z')) {
                move = r.nextInt(9);
            }
        }
        return move;
    }
    private int count(Cell [] cells, char ch){
        int count=0;
        for (Cell c : cells){
            if (c.getValue()==ch){
                count++;
            }
        }
        return count;
    }
    private int find(Cell[] cells, char ch){
        int position = -1;
        for (Cell c : cells){
            if (c.getValue()==ch){
                position = c.getI()*3+c.getJ();
                System.out.println("found i "+c.getI()+ " j="+c.getJ());
            }
        }
        return position;
    }
    private int onlyMove(Cell[] cells, char ch){
        int move = -1;
        if (count(cells, ch)==2&&count(cells,'z')==1){
            move = find(cells, 'z');
        }
        return move;
    }
    private int getOnlyMove( char ch ){
        int move = -1;
        for (int i=0; i<3; i++) {
            if (move==-1) move = onlyMove(getCol(i), ch);
            if (move==-1) move = onlyMove(getRow(i), ch);
        }
        if (move==-1) move = onlyMove(getDiag(0), ch);
        if (move==-1) move = onlyMove(getDiag(1), ch);
        System.out.println("onlyMove "+move);
        return move;
    }
    private int getPosNextToCorner(){
        int move=-1;
        if(find(getCorners(),'x')==0){
            if(boardStr.charAt(1)=='z') move = 1;
            if(boardStr.charAt(3)=='z') move = 3;
        }
         if(find(getCorners(),'x')==2&&move==-1){
             if(boardStr.charAt(1)=='z') move = 1;
             if(boardStr.charAt(5)=='z') move = 5;
        }
         if(find(getCorners(),'x')==6&&move==-1){
             if(boardStr.charAt(1)=='z') move = 1;
             if(boardStr.charAt(7)=='z') move = 7;
        }
         if(find(getCorners(),'x')==8&&move==-1){
             if(boardStr.charAt(5)=='z') move = 5;
             if(boardStr.charAt(7)=='z') move = 7;
        }
         return move;
    }

    public int moveToCordX(int move){
        return (move)%3;
    }
    public int moveToCordY(int move){
        return (move)/3;
    }
}
