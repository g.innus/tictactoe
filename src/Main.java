import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
    Settings settings = new Settings();
    settings.get();

    Game game = new Game(settings.isPlayersTurn(), settings.isTwoPlayer(), settings.getDifficulty(), settings.getSymbolHeight(), settings.getSymbolWith());
    game.start();



    }
}
