import java.util.Scanner;

public class Drawer {
    private int symbolHeight;
    private int symbolWith;
    private String pixelChar = Character.toString(0x2593);
    private String pixelOffChar = Character.toString(0x2591);
    //private String pixelChar = Character.toString(0x2588);
    private boolean pixOf = false;
    private int sizeY;
    private int sizeX;
    private int horizontalLine1Pos;
    private int horizontalLine2Pos;
    private int verticalLine1Pos;
    private int verticalLine2Pos;
    private boolean[][] screen;

    public void clear(){
        for (int i=0;i<12;i++){
            System.out.println();
        }
    }

    public Drawer(int symbolHeight, int symbolWith) {
        this.symbolHeight = symbolHeight;
        this.symbolWith = symbolWith;
        this.sizeY = (symbolHeight+2)*3+2;
        this.sizeX = (symbolWith+2)*3+4;
        this.horizontalLine1Pos = (sizeY - 2)/3;
        this.horizontalLine2Pos = (sizeY - 2)*2/3+1;
        this.verticalLine1Pos = (sizeX - 4)/3;
        this.verticalLine2Pos = (sizeX - 4)*2/3+2;
        this.screen = new boolean[sizeY][sizeX];
        for (int j=0; j<screen.length; j++){
            for (int k=0; k<screen[0].length; k++) {
                if (k == verticalLine1Pos||k==verticalLine1Pos+1 || k == verticalLine2Pos ||k==verticalLine2Pos+1|| j==horizontalLine1Pos || j==horizontalLine2Pos) {
                    screen[j][k] = !pixOf;
                } else {
                    screen[j][k] = pixOf;
                }
            }
        }
    }


    public void draw(){
        clear();
        String [] lines = new String[sizeY];
        for (int j=0; j<lines.length; j++){
            lines[j] = "";
        }
        for(int i=0; i<screen.length; i++){
            for(int j=0; j<screen[0].length; j++){
                if (screen[i][j]) {
                    lines[i] += pixelChar;
                } else {
                    if((i==symbolHeight+3||i==(symbolHeight+3)*2||i==0)&&(j==0||j==symbolWith+4||j==(symbolWith+4)*2)||
                            (i==symbolHeight+3||i==(symbolHeight+3)*2||i==0)&&(j==1||j==symbolWith+5||j==(symbolWith+4)*2+1)){
                        if(j==0||j==symbolWith+4||j==(symbolWith+4)*2) {
                            lines[i] += Integer.toString(i/(symbolHeight+2));
                        }else {
                            lines[i] += Integer.toString(j/(symbolWith+4));
                        }
                    }else {
                        lines[i] += pixelOffChar;
                    }
                }
            }
        }

        for (String line: lines){
            System.out.println("   "+line);
        }
    }
    public void update(Board board){
        for (int i=0;i<3;i++) {
            for (int j=0;j<3;j++){
                if(board.getCell(i, j).getValue()=='0'){
                    enterSymbol(j, i, 0);
                }
                if(board.getCell(i, j).getValue()=='X'){
                    enterSymbol(j, i, 1);
                }
            }
        }
    }
    private int getCellCenterHorizontal(int i){
        return (symbolWith+2-1)/2+i*(symbolWith+4);
    }
    private int getCellCenterVertical(int i){
        return (symbolHeight+2-1)/2+i*(symbolHeight+2)+i;
    }
    public void enterSymbol(int i, int j, int type){
        int nextLineOffse = symbolWith/symbolHeight;
        int centerX = getCellCenterHorizontal(i);
        int centerY = getCellCenterVertical(j);
        if(type==0){
            for (int k=0;k<symbolWith/2;k++) {
                screen[centerY-symbolHeight/2][centerX-nextLineOffse+k]=!pixOf;
                screen[centerY-symbolHeight/2][centerX+nextLineOffse-k]=!pixOf;
                screen[centerY+symbolHeight/2][centerX-nextLineOffse+k]=!pixOf;
                screen[centerY+symbolHeight/2][centerX+nextLineOffse-k]=!pixOf;
            }
            for (int k=0;k<symbolHeight/2+1;k++){
                screen[centerY+k-nextLineOffse/2][centerX-symbolWith/2]=!pixOf;
                screen[centerY-k+nextLineOffse/2][centerX-symbolWith/2]=!pixOf;
                screen[centerY+k-nextLineOffse/2][centerX+symbolWith/2]=!pixOf;
                screen[centerY-k+nextLineOffse/2][centerX+symbolWith/2]=!pixOf;
            }

        }else {
            screen[centerY][centerX]=!pixOf;
            for (int k=1;k<symbolHeight/2+1;k++){
                screen[centerY-k][centerX-nextLineOffse*k]=!pixOf;
                screen[centerY-k][centerX+nextLineOffse*k]=!pixOf;
                screen[centerY+k][centerX+nextLineOffse*k]=!pixOf;
                screen[centerY+k][centerX-nextLineOffse*k]=!pixOf;
            }

        }

    }


}
