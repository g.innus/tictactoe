import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {
    private Board board;
    private boolean gameFinished = false;
    private boolean playersTurn = true;
    private boolean twoPlayer = false;
    private int difficulty = 0;
    private int symbolHeihgt = 7;
    private int symbolWidth = 21;
    Scanner sc = new Scanner(System.in);
    private Drawer drawer;

    public Game(boolean playersTurn, boolean twoPlayer, int difficulty, int symbolHeihgt, int symbolWidth) {
        this.playersTurn = playersTurn;
        this.twoPlayer = twoPlayer;
        this.difficulty = difficulty;
        this.symbolHeihgt = symbolHeihgt;
        this.symbolWidth = symbolWidth;
        this.board = new Board();
        board.clearBoard();
        drawer = new Drawer(symbolHeihgt, symbolWidth);
        drawer.draw();
    }


    public void selectMode(){
        System.out.println("Select Game Mode:");
        System.out.println("1 Single Player");
        System.out.println("2 Two Player");
        String input = sc.next();
        Pattern inPattern = Pattern.compile("[1-2]");
        while (!inPattern.matcher(input).matches()){
            System.out.println("Select Game Mode:");
            System.out.println("1 Single Player");
            System.out.println("2 Two Player");
            input = sc.next();
        }
        if(input.matches("2")){
            twoPlayer=true;
        }

    }

    public void start() {
        while (!gameFinished) {
            drawer.update(board);
            drawer.draw();
            Bot bot = new Bot();
            bot.update(board);
            if (playersTurn&&!(anyoneWin()||board.full())) {
                playerMove();
            }
            if(!playersTurn&&!(anyoneWin()||board.full())){
                if(!twoPlayer) {
                    pcMove(difficulty);
                }else {
                    playerMove();
                }
            }
            gameFinished=(anyoneWin()||board.full());
            playersTurn = !playersTurn;
        }
        drawer.update(board);
        drawer.draw();
        System.out.println(getWinner());
    }

    public String getWinner(){
        String out;
        if (board.full()&&!anyoneWin()) {
            out = "Sorry, no winer!";
        } else {
            if(twoPlayer){
                if(playersTurn){
                    out = "Player 2 Wins!!";
                }else {
                    out = "Player 1 Wins!!";
                }
            }else {
                if(playersTurn){
                    out = "You loose!!!";
                }else {
                    out = "You WIN!!!";
                }
            }
        }
        return out;
    }

    private void sysOutPlayer(){
        if (twoPlayer){
            if (playersTurn) {
                System.out.println("Your turn Player 1");
            } else {
                System.out.println("Your turn Player 2");
            }
        }else {
            System.out.println("Your turn!");
        }
        System.out.println("Enter cotdinates:");
    }

    private void playerMove(){
        sysOutPlayer();
        String input = sc.next();
        while (!coordinatesAlowed(input)||!board.cellEmpty(coordinateStrToInt(0, input), coordinateStrToInt(1,input))){
            System.out.println("Bad cordinates entered! Enter new cotdinates");
            sysOutPlayer();
            input = sc.next();
        }
        int row = coordinateStrToInt(0,input);
        int col = coordinateStrToInt(1,input);
        board.setCell(row, col, playersTurn);
    }
    private void pcMove(int difficulty){
        Bot bot = new Bot();
        bot.update(board);
        int move = bot.getNextMove(difficulty);
        board.setCell(bot.moveToCordY(move), bot.moveToCordX(move), false);
    }

    private boolean coordinatesAlowed(String input){
        boolean allowed=true;
        Pattern inPattern = Pattern.compile("[0-2][0-2]");
        Matcher m = inPattern.matcher(input);
        allowed = m.matches();
        return allowed;
    }

    private boolean anyoneWin(){
        boolean finished = false;
        if(board.anyRowWin()||board.anyColWin()||board.diagWin()){
            finished=true;
        }
        return finished;
    }

    private int coordinateStrToInt(int pos, String coordinates){
        Pattern inPattern = Pattern.compile("([0-2])");
        Matcher m = inPattern.matcher(coordinates);
        m.find();
        if(pos>0){
            m.find();
        }
        return Integer.parseInt(m.group());
    }

}





