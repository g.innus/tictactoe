import java.util.Arrays;

public class Board {
    private Cell[][] board= new Cell[3][3];

    public Cell[][] getBoard() {
        return board;
    }
    public Cell getCell(int i, int j){
        return board[i][j];
    }

    public void clearBoard(){
        for (int i=0;i<3;i++) {
            for (int j=0;j<3;j++){
                board[i][j] = new Cell();
                board[i][j].setI(i);
                board[i][j].setJ(j);
            }
        }
    }


    public void setCell(int i, int j, boolean player){
        if (player) {
            board[i][j].setValue('X');
        }else {
            board[i][j].setValue('0');
        }
    }
    public boolean cellEmpty(int i, int j){
        if(board[i][j].getValue() == ' '){
            return true;
        }else {
            return false;
        }
    }


    public boolean rowWin(int i){
        boolean win = true;
        for (int j=0; j<3; j++){
            if(board[i][0].getValue()==' '){
                win=false;
            }
            if(board[i][j].getValue()!=board[i][0].getValue()){
                win=false;
            }
        }
        return win;
    }

    public boolean anyRowWin(){
        boolean win = false;
        for (int i=0; i<3; i++) {
            if(rowWin(i)){
                win=true;
            }
        }
        return win;
    }

    public boolean anyColWin(){
        boolean win = false;
        for (int i=0; i<3; i++) {
            if(colWin(i)){
                win=true;
            }
        }
        return win;
    }


    public boolean colWin(int i){
        boolean win = true;
        for (int j=0; j<3; j++){
            if(board[0][i].getValue()==' '){
                win=false;
            }
            if(board[j][i].getValue()!=board[0][i].getValue()){
                win=false;
            }
        }
        return win;
    }

    public boolean diagWin(){
        boolean win = false;
        if(board[1][1].getValue()!=' '){
            if ((board[1][1].getValue()==board[0][0].getValue()&&board[1][1].getValue()==board[2][2].getValue())) {
                win = true;
            }
            if ((board[1][1].getValue()==board[2][0].getValue()&&board[1][1].getValue()==board[0][2].getValue())) {
                win = true;
            }
        }
        return win;
    }

    public boolean full(){
        boolean full = true;
        for (int i=0; i<3; i++){
            for (int j=0; j<3; j++){
                if (board[i][j].getValue()==' ') {
                    full=false;
                }
            }
        }
        return full;
    }



}
