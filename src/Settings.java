import java.util.Scanner;

public class Settings {
    private boolean playersTurn = true;
    private boolean twoPlayer = false;
    private int difficulty = 0;
    private int symbolHeight = 5;
    private int symbolWith = 13;
    private boolean done = false;

    public boolean isPlayersTurn() {
        return playersTurn;
    }

    public boolean isTwoPlayer() {
        return twoPlayer;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public int getSymbolHeight() {
        return symbolHeight;
    }

    public int getSymbolWith() {
        return symbolWith;
    }

    public void display(){
        System.out.println("Settings");
        System.out.println();
        System.out.println();

        System.out.print("1. Game Mode: ");
        if (twoPlayer) {
            System.out.println("Two player");
        }else{
            System.out.println("Single player");
        }

        System.out.print("2. First move: ");
        if (playersTurn) {
            System.out.println("Player 1");
        }else {
            System.out.println("Player 2 / PC");
        }

        System.out.print("3. Difficulty: ");
        if (difficulty==0){
            System.out.println("Easy");
        }else {
            System.out.println("Hard");
        }

        System.out.print("4. Display size: ");
        if (symbolWith==5) {
            System.out.println("Small");
        }
        if (symbolWith==13) {
            System.out.println("Medium");
        }
        if (symbolWith==17) {
            System.out.println("Large");
        }
        System.out.println();
        System.out.println("5. START GAME");
        System.out.println();
        System.out.println("Enetr selection:");

    }
    public void get(){
        Scanner scanner = new Scanner(System.in);
        do {
            new Drawer(symbolHeight, symbolWith).draw();
            display();
            switch (scanner.nextInt()) {
                case 1:
                    System.out.println("Select Game Mode");
                    System.out.println("1. Single Player");
                    System.out.println("2. Two Player");
                    switch (scanner.nextInt()) {
                        case 1:
                            twoPlayer = false;
                            display();
                            break;
                        case 2:
                            twoPlayer = true;
                            display();
                            break;
                        default:
                            display();
                            break;
                    }
                    break;
                case 2:
                    System.out.println("Select First Move");
                    System.out.println("1. Player 1");
                    System.out.println("2. Player 2 / PC");
                    switch (scanner.nextInt()) {
                        case 1:
                            playersTurn = true;
                            display();
                            break;
                        case 2:
                            playersTurn = false;
                            display();
                            break;
                        default:
                            display();
                            break;
                    }
                    break;
                    case 3:
                    System.out.println("Select Difficulty");
                    System.out.println("1. Easy");
                    System.out.println("2. Hard");
                    switch (scanner.nextInt()) {
                        case 1:
                            difficulty = 0;
                            display();
                            break;
                        case 2:
                            difficulty = 1;
                            display();
                            break;
                        default:
                            display();
                            break;
                    }
                    break;
                    case 4:
                    System.out.println("Display size:");
                    System.out.println("1. Small");
                    System.out.println("2. Medium");
                    System.out.println("3. Large");
                    switch (scanner.nextInt()) {
                        case 1:
                            symbolHeight = 3;
                            symbolWith = 5;
                            display();
                            break;
                        case 2:
                            symbolHeight = 5;
                            symbolWith = 13;
                            display();
                            break;
                        case 3:
                            symbolHeight = 5;
                            symbolWith = 19;
                            display();
                            break;
                        default:
                            display();
                            break;
                    }
                    break;

                    default:
                    done = true;
            }
        } while (done==false);
    }
}
